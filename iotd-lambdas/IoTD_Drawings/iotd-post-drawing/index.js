var async = require('async'),
    AWS = require('aws-sdk'),
    constants = require('./constants');

exports.handler = function (event, context) {

    console.log('event: ', event);

    AWS.config.update(constants.dynamodb);
    var dynamodb = new AWS.DynamoDB();

    function init(callbackInit) {

        var params = {
            Item: {
                id: {
                    S: event.id
                },
                name: {
                    S: event.name
                },
                author: {
                    S: event.author
                },
                date: {
                    N: event.date
                }
            },
            TableName: 'IoTD_Drawings'
        };
        return callbackInit(null, params);
    }

    function createDrawing(params, callbackCreateDrawing) {

        dynamodb.putItem(params, function (err, data) {
            if (err) return callbackCreateDrawing(err.stack);
            callbackCreateDrawing(null, data)
        });
    }

    async.waterfall([
        init,
        createDrawing
    ], function (err, result) {
        if (err) {
            console.log(err);
            return context.fail(err);
        }
        return context.succeed({success: 'Created'});
    });
};

/*
var f = function (mess) {
    return console.log(mess);
};

var context = {
    succeed: f,
    fail: f
};

exports.handler({
    id: "12345qwerty1",
    name: "test 2",
    author: "J2",
    date: "12345678901"
}, context);
*/