var async = require('async'),
    AWS = require('aws-sdk'),
    utils = require('./utils'),
    constants = require('./constants');

exports.handler = function (event, context) {

    console.log('event: ', event);

    AWS.config.update(constants.dynamodb);
    var dynamodb = new AWS.DynamoDB();

    function init(callbackInit) {
        var params = {
            TableName: 'IoTD_Drawings'
        };
        return callbackInit(null, params);
    }

    function getDrawings(params, callbackGetDrawings) {

        dynamodb.scan(params, function (err, data) {
            if (err) return callbackGetDrawings(err.stack);

            var drawings = [];
            if (data && data.Items) drawings = utils.parseToDictionary(data.Items);
            callbackGetDrawings(null, drawings);
        });
    }

    async.waterfall([
        init,
        getDrawings
    ], function (err, result) {
        if (err) {
            console.log(err);
            return context.fail(err);
        }
        return context.succeed(result);
    });
};

/*
var f = function (mess) {
    return console.log(mess);
};

var context = {
    succeed: f,
    fail: f
};

exports.handler({}, context);
*/