var async = require('async'),
    AWS = require('aws-sdk'),
    constants = require('./constants');

exports.handler = function (event, context) {

    console.log('event: ', event);

    AWS.config.update(constants.dynamodb);
    var dynamodb = new AWS.DynamoDB();

    function init(callbackInit) {

        if (!event.topic) return callbackInit('Empty topic');
        if (!event.message) return callbackInit('Empty message');

        var topicArray = event.topic.split("/");
        if (topicArray.length != 3) return callbackInit('Broken topic');

        var drawing_id = topicArray[2];

        var params = {
            Item: {
                line_id: {
                    S: event.message.line_id
                },
                drawing_id: {
                    S: drawing_id
                },
                x: {
                    N: event.message.x
                },
                y: {
                    N: event.message.y
                },
                color: {
                    S: event.message.color
                },
                nano_time: {
                    N: event.message.nano_time
                }
            },
            TableName: 'IoTD_Points'
        };
        return callbackInit(null, params);
    }

    function createPoint(params, callbackCreatePoint) {

        dynamodb.putItem(params, function (err, data) {
            if (err) return callbackCreatePoint(err.stack);
            callbackCreatePoint(null, data)
        });
    }

    async.waterfall([
        init,
        createPoint
    ], function (err, result) {
        if (err) {
            console.log(err);
            return context.fail(err);
        }
        return context.succeed({success: 'Created'});
    });
};

/*
var f = function (mess) {
    return console.log(mess);
};

var context = {
    succeed: f,
    fail: f
};

exports.handler({
    line_id: "12345qwerty1",
    drawing_id: "567890987654",
    x: "3",
    y: "4",
    color: "000344",
    nano_time: "12345678901"
}, context);
*/