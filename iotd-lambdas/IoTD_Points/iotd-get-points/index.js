var async = require('async'),
    AWS = require('aws-sdk'),
    utils = require('./utils'),
    constants = require('./constants');

exports.handler = function (event, context) {

    console.log('event: ', event);

    AWS.config.update(constants.dynamodb);
    var dynamodb = new AWS.DynamoDB();

    function init(callbackInit) {

        if (!event.drawing_id) return callbackInit('Empty drawing_id');

        var params = {
            IndexName: 'drawing_id-index',
            TableName: 'IoTD_Points',
            KeyConditions: {
                drawing_id: {
                    ComparisonOperator: 'EQ',
                    AttributeValueList: [
                        {
                            S: event.drawing_id
                        }
                    ]
                }
            }
        };
        return callbackInit(null, params);
    }

    function getPoints(params, callbackGetPoints) {

        dynamodb.query(params, function (err, data) {
            if (err) return callbackGetPoints(err.stack);
            var points = [];
            if (data && data.Items) points = utils.parseToDictionary(data.Items);
            callbackGetPoints(null, points);
        });
    }

    async.waterfall([
        init,
        getPoints
    ], function (err, result) {
        if (err) {
            console.log(err);
            return context.fail(err);
        }
        return context.succeed(result);
    });
};

/*
var f = function (mess) {
    return console.log(mess);
};

var context = {
    succeed: f,
    fail: f
};

exports.handler({
    drawing_id: "567890987654"
}, context);
*/