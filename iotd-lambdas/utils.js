function parseToDictionary (items) {
    var result = [];
    if (items.length == 0) return result;

    items.forEach(function(item) {

        var newItem = {};
        Object.keys(item).forEach(function(key) {
            var pair = item[key];
            newItem[key] = pair.S || pair.N;
        });
        result.push(newItem);
    });
    return result;
}

module.exports = {
    parseToDictionary: parseToDictionary
};